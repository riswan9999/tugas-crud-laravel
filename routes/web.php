<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/','IndexController@home');

Route::get('/register','AuthController@register');

Route::post('/welcome','AuthController@kirim');

Route::get('/data-tables','IndexController@table');

Route::get('/master', function () {
    return view('layout.master');
});

//CRUD KATEGORI
//CREATE
Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store' );

//READ
Route::get('/cast','CastController@index');
Route::get('/cast/{id}','CastController@show');

//UPDATE
Route::get('/cast/{id}/edit','CastController@edit');
Route::put('/cast/{id}','CastController@update');

//DELETE
Route::delete('/cast/{id}','CastController@destroy');
